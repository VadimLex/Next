//
//  Libra.cpp
//  Libra
//
//  Created by Vadim Aitov on 26.04.2022.
//

#include <iostream>
#include "Libra.hpp"
#include "LibraPriv.hpp"

void Libra::HelloWorld(const char * s)
{
    LibraPriv *theObj = new LibraPriv;
    theObj->HelloWorldPriv(s);
    delete theObj;
};

void LibraPriv::HelloWorldPriv(const char * s) 
{
    std::cout << s << std::endl;
};

