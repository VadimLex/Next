//
//  Libra.hpp
//  Libra
//
//  Created by Vadim Aitov on 26.04.2022.
//

#ifndef Libra_
#define Libra_

/* The classes below are exported */
#pragma GCC visibility push(default)

class Libra
{
    public:
    void HelloWorld(const char *);
};

#pragma GCC visibility pop
#endif
