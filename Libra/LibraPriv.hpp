//
//  LibraPriv.hpp
//  Libra
//
//  Created by Vadim Aitov on 26.04.2022.
//

/* The classes below are not exported */
#pragma GCC visibility push(hidden)

class LibraPriv
{
    public:
    void HelloWorldPriv(const char *);
};

#pragma GCC visibility pop
