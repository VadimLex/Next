//
//  main.cpp
//  Next
//
//  Created by Vadim Aitov on 18.04.2022.
//
/*
        Тестовый проект
 */
#include <iostream>

int GlobalSum = 0;

// These statement prints void - ничего не выводит
void Print()
{
    std::cout << "Helo SkillBox\n";
}
// Функция integer print выводит 10
int Print10()
{
    std::cout << "10\n";
    return 2;
}
//
int PrintInt(int ToPrint)
{
    std::cout << ToPrint << "\n ";
    return ToPrint;
}
// Сумма
long long Sum(long long a, long long b, long long c) {
    return (a+b+c);
}

// Главная функция main - НЕЛьзя писать Main!!!

int main()
{
    int x=100, y=x+100;
//    int y=x+100;
    int mult=x*y;
    int random=1001;
    // Вызываем оператор << на объекте типа std::cout
    std::cout << "Результаты теста: \n";
    std::cout << "Вывод random = " << random;
    std::cout << "\nВывод mult = "   << mult << "\n";
    //MARK: - tesn mark
    Print();
    
    //Вывод только потока без return

    Print10();
    PrintInt(40);
    
    //Вывод  потока и return!!!
    
    int d=10, f=12;
    
    std::cout << Print10() << "\n ";
    std::cout << PrintInt(30) << "\n ";
    std::cout << Sum(d, f, GlobalSum) << "\n ";
    
//    Sum(a:1, b:2); - так в xcode не работает!
//
// Выводит хз какое значение return!!!
//    return Sum(8888888888, 509999999990);
    
//    Цикл
    for(int i=0; i<10; i++)
    {
        std::cout << i << "\n";
    }
    return 1;
}
